#import "CameraViewController.h"
#import "CameraViewProtocol.h"

@interface CameraViewController () <CameraViewProtocol>

@property (nonatomic, strong) id<CameraPresenterProtocol> presenter;

@end

@implementation CameraViewController
@synthesize presenter;

- (instancetype)initWithPresenter: (id<CameraPresenterProtocol>)presenter {
    if (self = [super initWithNibName:NSStringFromClass(CameraViewController.class) bundle:nil]) {
        self.presenter = presenter;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [presenter attachView:self];
}

- (void)showPreviewLayer:(AVCaptureVideoPreviewLayer *)previewLayer {
    previewLayer.frame = self.view.bounds;
    [self.view.layer insertSublayer:previewLayer atIndex:0];
    
    [presenter startRunning];
}

- (void)showErrorWithTitle:(id)title messaje:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Aceptar" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)openSettingsApp {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
}

@end
