#import "CameraPresenterProtocol.h"
#import "PermissionsManager.h"
#import "CaptureSessionCoordinator.h"

NS_ASSUME_NONNULL_BEGIN

@interface CameraPresenter : NSObject<CameraPresenterProtocol>

- (instancetype)initWithPermissionManager:(PermissionsManager *)permissionManager
                captureSessionCoordinator:(CaptureSessionCoordinator *)captureSessionCoordinator;

@end

NS_ASSUME_NONNULL_END
