#import "CameraViewProtocol.h"

@protocol CameraPresenterProtocol

- (void)attachView:(id<CameraViewProtocol>)view;
- (void)startRunning;
- (void)stopRunning;
- (void)toggleTorch;

@end
