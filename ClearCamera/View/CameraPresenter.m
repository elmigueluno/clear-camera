#import "CameraPresenter.h"
#import "CameraViewProtocol.h"

@interface CameraPresenter ()

@property (nonatomic, weak) id<CameraViewProtocol> view;
@property (nonatomic, strong) PermissionsManager *permissionsManager;
@property (nonatomic, strong) CaptureSessionCoordinator *captureSessionCoordinator;
@property (nonatomic) BOOL cameraGranted;
@property (nonatomic) BOOL microphoneGranted;

@end

@implementation CameraPresenter
@synthesize view, permissionsManager, captureSessionCoordinator, cameraGranted, microphoneGranted;

- (instancetype)init {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"%s is not a valid initializer", __func__]
                                 userInfo:nil];
}

- (instancetype)initWithPermissionManager:(PermissionsManager *)permissionManager
                captureSessionCoordinator:(CaptureSessionCoordinator *)captureSessionCoordinator {
    if (self = [super init]) {
        self.permissionsManager = permissionManager;
        self.captureSessionCoordinator = captureSessionCoordinator;
    }
    
    return self;
}

- (void)attachView:(id<CameraViewProtocol>)view {
    self.view = view;
    
    [self getPermissions];
}

- (void)getPermissions {
    [permissionsManager cameraAuthorizationStatusWithCompletionHandler:^(PermissionModel *response) {
        self->cameraGranted = response.granted;
        if (!response.granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->view showErrorWithTitle:response.title messaje:response.message];
            });
        } else {
            [self->permissionsManager microphonePermissionsWithCompletionHandler:^(PermissionModel *response) {
                self->microphoneGranted = response.granted;
                if (!response.granted) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self->view showErrorWithTitle:response.title messaje:response.message];
                    });
                }
            }];
        }
    }];
}

- (void)startRunning {
    [captureSessionCoordinator startRunning];
}

- (void)stopRunning {
    [captureSessionCoordinator stopRunning];
}

- (void)toggleTorch {
    [captureSessionCoordinator toggleTorch];
}

@end
