#import <UIKit/UIKit.h>
#import "CameraPresenterProtocol.h"

@interface CameraViewController : UIViewController

NS_ASSUME_NONNULL_BEGIN

- (instancetype)initWithPresenter: (id<CameraPresenterProtocol>)presenter;

@end

NS_ASSUME_NONNULL_END
