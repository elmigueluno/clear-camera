#import <AVFoundation/AVFoundation.h>

@protocol CameraViewProtocol

- (void)showPreviewLayer:(AVCaptureVideoPreviewLayer *)previewLayer;
- (void)showErrorWithTitle:(NSString *)title messaje:(NSString *)message;
- (void)openSettingsApp;

@end
