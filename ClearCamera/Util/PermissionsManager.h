#import <Foundation/Foundation.h>
#import "PermissionModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PermissionsManager : NSObject

- (void)microphonePermissionsWithCompletionHandler:(void(^)(PermissionModel *response))completionHandler;
- (void)cameraAuthorizationStatusWithCompletionHandler:(void(^)(PermissionModel *response))completionHandler;

@end

NS_ASSUME_NONNULL_END
