#import "PermissionsManager.h"
#import <AVFoundation/AVFoundation.h>


@interface PermissionsManager ()

@end

@implementation PermissionsManager

- (void)microphonePermissionsWithCompletionHandler:(void(^)(PermissionModel *response))completionHandler {
    NSString *mediaType = AVMediaTypeAudio;
    [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
        PermissionModel *model = [[PermissionModel alloc] initWithGranted:granted];
        if (!granted) {
            model.title = @"Microphone Disabled";
            model.message = @"To enable sound recording with your video please go to the Settings app > Privacy > Microphone and enable access.";
        }
        
        completionHandler(model);
    }];
}


- (void)cameraAuthorizationStatusWithCompletionHandler:(void(^)(PermissionModel *response))completionHandler {
	NSString *mediaType = AVMediaTypeVideo;
    [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
        PermissionModel *model = [[PermissionModel alloc] initWithGranted:granted];
        if (!granted){
            model.title = @"Camera disabled";
            model.message = @"This app doesn't have permission to use the camera, please go to the Settings app > Privacy > Camera and enable access.";
        }
        
        completionHandler(model);
    }];
}

@end
