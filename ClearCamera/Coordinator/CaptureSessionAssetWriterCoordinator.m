#import "CaptureSessionAssetWriterCoordinator.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "AssetWriterCoordinator.h"
#import "FileManager.h"


typedef NS_ENUM(NSInteger, RecordingStatus) {
    RecordingStatusIdle = 0,
    RecordingStatusStartingRecording,
    RecordingStatusRecording,
    RecordingStatusStoppingRecording,
}; // internal state machine


@interface CaptureSessionAssetWriterCoordinator () <AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate, AssetWriterCoordinatorDelegate>

@property (nonatomic, strong) dispatch_queue_t videoDataOutputQueue;
@property (nonatomic, strong) dispatch_queue_t audioDataOutputQueue;

@property (nonatomic, strong) AVCaptureVideoDataOutput *videoDataOutput;
@property (nonatomic, strong) AVCaptureAudioDataOutput *audioDataOutput;

@property (nonatomic, strong) AVCaptureConnection *audioConnection;
@property (nonatomic, strong) AVCaptureConnection *videoConnection;

@property (nonatomic, strong) NSDictionary *videoCompressionSettings;
@property (nonatomic, strong) NSDictionary *audioCompressionSettings;

@property (nonatomic, strong) AVAssetWriter *assetWriter;

@property (nonatomic, assign) RecordingStatus recordingStatus;
@property (nonatomic, assign) BOOL recordingAudioStatus;
@property (nonatomic, strong) NSURL *recordingURL;

@property(nonatomic, retain) __attribute__((NSObject)) CMFormatDescriptionRef outputVideoFormatDescription;
@property(nonatomic, retain) __attribute__((NSObject)) CMFormatDescriptionRef outputAudioFormatDescription;
@property(nonatomic, retain) AssetWriterCoordinator *assetWriterCoordinator;

@end

@implementation CaptureSessionAssetWriterCoordinator
@synthesize videoDataOutputQueue, audioDataOutputQueue, videoDataOutput, audioDataOutput, audioConnection, videoConnection, videoCompressionSettings, audioCompressionSettings, recordingStatus, recordingAudioStatus, recordingURL, outputAudioFormatDescription, assetWriterCoordinator;


- (instancetype)init {
    self = [super init];
     if (self) {
        self.videoDataOutputQueue = dispatch_queue_create("com.example.capturesession.videodata", DISPATCH_QUEUE_SERIAL);
        dispatch_set_target_queue(videoDataOutputQueue, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0));
        self.audioDataOutputQueue = dispatch_queue_create("com.example.capturesession.audiodata", DISPATCH_QUEUE_SERIAL);
        [self addDataOutputsToCaptureSession:self.captureSession];

    }
    return self;
}

#pragma mark - Recording

- (void)startRecording {
    [super toggleTorch];
    @synchronized(self)
    {
         if (recordingStatus != RecordingStatusIdle) {
            @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"Already recording" userInfo:nil];
            return;
        }
        [self transitionToRecordingStatus:RecordingStatusStartingRecording error:nil];
    }
    
    FileManager *fm = [FileManager new];
    recordingURL = [fm tempFileURL];
    

    self.assetWriterCoordinator = [[AssetWriterCoordinator alloc] initWithURL:recordingURL];
     if (outputAudioFormatDescription != nil) {
        [assetWriterCoordinator addAudioTrackWithSourceFormatDescription:self.outputAudioFormatDescription settings:audioCompressionSettings];
    }
    [assetWriterCoordinator addVideoTrackWithSourceFormatDescription:self.outputVideoFormatDescription settings:videoCompressionSettings];
    
    dispatch_queue_t callbackQueue = dispatch_queue_create("com.example.capturesession.writercallback", DISPATCH_QUEUE_SERIAL); // guarantee ordering of callbacks with a serial queue
    [assetWriterCoordinator setDelegate:self callbackQueue:callbackQueue];
    [assetWriterCoordinator prepareToRecord]; // asynchronous, will call us back with recorderDidFinishPreparing: or recorder:didFailWithError: when done
}

- (void)stopRecording {
    [super toggleTorch];
    @synchronized(self)
    {
        if (recordingStatus != RecordingStatusRecording) {
            return;
        }
        [self transitionToRecordingStatus:RecordingStatusStoppingRecording error:nil];
    }
    [self.assetWriterCoordinator finishRecording]; // asynchronous, will call us back with
}

- (void)startRecordingAudio {
    recordingAudioStatus = YES;
    NSLog(@"startRecordingAudio");
}

- (void)stopRecordingAudio {
    recordingAudioStatus = NO;
    NSLog(@"stopRecordingAudio");
}

#pragma mark - Private methods

- (void)addDataOutputsToCaptureSession:(AVCaptureSession *)captureSession {
    videoDataOutput = [AVCaptureVideoDataOutput new];
    videoDataOutput.videoSettings = nil;
    videoDataOutput.alwaysDiscardsLateVideoFrames = NO;
    [videoDataOutput setSampleBufferDelegate:self queue:videoDataOutputQueue];
    
    audioDataOutput = [AVCaptureAudioDataOutput new];
    [audioDataOutput setSampleBufferDelegate:self queue:audioDataOutputQueue];
   
    [self addOutput:videoDataOutput toCaptureSession:self.captureSession];
    videoConnection = [videoDataOutput connectionWithMediaType:AVMediaTypeVideo];
    
    [self addOutput:audioDataOutput toCaptureSession:self.captureSession];
    audioConnection = [audioDataOutput connectionWithMediaType:AVMediaTypeAudio];
    
    [self setCompressionSettings];
}

- (void)setupVideoPipelineWithInputFormatDescription:(CMFormatDescriptionRef)inputFormatDescription {
    self.outputVideoFormatDescription = inputFormatDescription;
}

- (void)teardownVideoPipeline {
    self.outputVideoFormatDescription = nil;
}

- (void)setCompressionSettings {
    videoCompressionSettings = [videoDataOutput recommendedVideoSettingsForAssetWriterWithOutputFileType:AVFileTypeQuickTimeMovie];
    audioCompressionSettings = [audioDataOutput recommendedAudioSettingsForAssetWriterWithOutputFileType:AVFileTypeQuickTimeMovie];
}

#pragma mark - SampleBufferDelegate methods

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    CMFormatDescriptionRef formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer);
    
    if (connection == videoConnection) {
        if (self.outputVideoFormatDescription == nil) {
            // Don't render the first sample buffer.
            // This gives us one frame interval (33ms at 30fps) for setupVideoPipelineWithInputFormatDescription: to complete.
            // Ideally this would be done asynchronously to ensure frames don't back up on slower devices.
            
            //TODO: outputVideoFormatDescription should be updated whenever video configuration is changed (frame rate, etc.)
            //Currently we don't use the outputVideoFormatDescription in IDAssetWriterRecoredSession
            [self setupVideoPipelineWithInputFormatDescription:formatDescription];
        } else {
            self.outputVideoFormatDescription = formatDescription;
            @synchronized(self) {
                 if (recordingStatus == RecordingStatusRecording) {
                    [assetWriterCoordinator appendVideoSampleBuffer:sampleBuffer];
                }
            }
        }
    } else if (connection == audioConnection) {
        self.outputAudioFormatDescription = formatDescription;
        @synchronized(self) {
             if (recordingStatus == RecordingStatusRecording) {
                 if (!recordingAudioStatus) {
                    [self muteAudioInBuffer:sampleBuffer];
                }
                [assetWriterCoordinator appendAudioSampleBuffer:sampleBuffer];
            }
        }
    }
}

//
//
//
//
//


- (void) muteAudioInBuffer:(CMSampleBufferRef)sampleBuffer {
    CMItemCount numSamples = CMSampleBufferGetNumSamples(sampleBuffer);
    NSUInteger channelIndex = 0;
    
    CMBlockBufferRef audioBlockBuffer = CMSampleBufferGetDataBuffer(sampleBuffer);
    size_t audioBlockBufferOffset = (channelIndex * numSamples * sizeof(SInt16));
    size_t lengthAtOffset = 0;
    size_t totalLength = 0;
    SInt16 *samples = NULL;
    CMBlockBufferGetDataPointer(audioBlockBuffer, audioBlockBufferOffset, &lengthAtOffset, &totalLength, (char **)(&samples));
    
    for (NSInteger i=0; i<numSamples; i++) {
        samples[i] = (SInt16)0;
    }
}

//
//
//
//

#pragma mark - IDAssetWriterCoordinatorDelegate methods

- (void)writerCoordinatorDidFinishPreparing:(AssetWriterCoordinator *)coordinator {
    @synchronized(self)
    {
         if (recordingStatus != RecordingStatusStartingRecording) {
            @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"Expected to be in StartingRecording state" userInfo:nil];
            return;
        }
        [self transitionToRecordingStatus:RecordingStatusRecording error:nil];
    }
}

- (void)writerCoordinator:(AssetWriterCoordinator *)recorder didFailWithError:(NSError *)error {
    @synchronized(self) {
        self.assetWriterCoordinator = nil;
        [self transitionToRecordingStatus:RecordingStatusIdle error:error];
    }
}

- (void)writerCoordinatorDidFinishRecording:(AssetWriterCoordinator *)coordinator {
    @synchronized(self)
    {
        if (recordingStatus != RecordingStatusStoppingRecording) {
            @throw [NSException exceptionWithName:NSInternalInconsistencyException reason:@"Expected to be in StoppingRecording state" userInfo:nil];
            return;
        }
        // No state transition, we are still in the process of stopping.
        // We will be stopped once we save to the assets library.
    }
    
    self.assetWriterCoordinator = nil;
    
    @synchronized(self) {
        [self transitionToRecordingStatus:RecordingStatusIdle error:nil];
    }
}


#pragma mark - Recording State Machine

// call under @synchonized(self)
- (void)transitionToRecordingStatus:(RecordingStatus)newStatus error:(NSError *)error {
    RecordingStatus oldStatus = recordingStatus;
    recordingStatus = newStatus;
    
    if (newStatus != oldStatus) {
        if (error && (newStatus == RecordingStatusIdle)) {
            dispatch_async(self.delegateCallbackQueue, ^{
                @autoreleasepool {
                    [self.delegate coordinator:self didFinishRecordingToOutputFileURL:self->recordingURL error:nil];
                }
            });
        } else {
            error = nil; // only the above delegate method takes an error
            if (oldStatus == RecordingStatusStartingRecording && newStatus == RecordingStatusRecording) {
                dispatch_async(self.delegateCallbackQueue, ^{
                    @autoreleasepool {
                        [self.delegate coordinatorDidBeginRecording:self];
                    }
                });
            } else if (oldStatus == RecordingStatusStoppingRecording && newStatus == RecordingStatusIdle) {
                dispatch_async(self.delegateCallbackQueue, ^{
                    @autoreleasepool {
                        [self.delegate coordinator:self didFinishRecordingToOutputFileURL:self->recordingURL error:nil];
                    }
                });
            }
        }
    }
}

@end
