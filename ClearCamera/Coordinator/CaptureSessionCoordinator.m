#import "CaptureSessionCoordinator.h"

@interface CaptureSessionCoordinator ()

@property (nonatomic, strong) dispatch_queue_t sessionQueue;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;
@property (nonatomic, strong) AVCaptureDeviceInput *videoDeviceInput;
@property (nonatomic, strong) NSArray<AVCaptureDevice *> *videoDevices;

@end

@implementation CaptureSessionCoordinator
@synthesize sessionQueue, previewLayer, delegate, videoDeviceInput, videoDevices, delegateCallbackQueue, captureSession, cameraDevice;


- (instancetype)init {
    self = [super init];
    if (self) {
        sessionQueue = dispatch_queue_create("com.example.capturepipeline.session", DISPATCH_QUEUE_SERIAL);
        captureSession = [self setupCaptureSession];
        NSArray *deviceTypes = @[AVCaptureDeviceTypeBuiltInWideAngleCamera,
                                 AVCaptureDeviceTypeBuiltInTelephotoCamera];
        AVCaptureDeviceDiscoverySession *captureDeviceDiscoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:deviceTypes mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionUnspecified];
        videoDevices = [captureDeviceDiscoverySession devices];
    }
    return self;
}

- (void)setDelegate:(id<CaptureSessionCoordinatorDelegate>)delegate callbackQueue:(dispatch_queue_t)delegateCallbackQueue {
    if (delegate && (delegateCallbackQueue == NULL)) {
        @throw [NSException exceptionWithName:NSInvalidArgumentException reason:@"Caller must provide a delegateCallbackQueue" userInfo:nil];
    }
    @synchronized (self) {
        delegate = delegate;
        if (self.delegateCallbackQueue != delegateCallbackQueue){
            self.delegateCallbackQueue = delegateCallbackQueue;
        }
    }
}

- (AVCaptureVideoPreviewLayer *)previewLayer {
    if (!previewLayer && captureSession) {
        previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:captureSession];
    }
    return previewLayer;
}

- (void)startRunning {
    dispatch_sync(sessionQueue, ^{
        [self->captureSession startRunning];
    });
}

- (void)stopRunning {
    dispatch_sync(sessionQueue, ^{
        // the captureSessionDidStopRunning method will stop recording if necessary as well, but we do it here so that the last video and audio samples are better aligned
        [self stopRecording]; // does nothing if we aren't currently recording
        [self->captureSession stopRunning];
    });
}

- (void)toggleTorch {
    if ([cameraDevice hasTorch]) {
        NSError *error;
        [cameraDevice lockForConfiguration:&error];
        if (!error) {
            if (![cameraDevice isTorchActive]) {
                [cameraDevice setTorchModeOnWithLevel:0.1 error:&error];
                
                if (error) {
                    NSLog(@"%s setTorchModeOnWithLevel error: %@", __func__, error.description);
                }
            } else {
                cameraDevice.torchMode = AVCaptureTorchModeOff;
            }
        } else {
            NSLog(@"%s lockForConfigurationTorch error: %@", __func__, error.description);
        }
        [cameraDevice unlockForConfiguration];
    }
}

- (void)startRecording {
    //overwritten by subclass
}

- (void)stopRecording {
    //overwritten by subclass
}

- (void)startRecordingAudio {
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"%s is not a valid initializer", __func__]
                                 userInfo:nil];
}

- (void)stopRecordingAudio {
    
}



#pragma mark - Capture Session Setup


- (AVCaptureSession *)setupCaptureSession {
    AVCaptureSession *captureSession = [AVCaptureSession new];
    
    if (![self addCameraAtPosition:AVCaptureDevicePositionBack toCaptureSession:captureSession]) {
    //if (![self addDefaultCameraInputToCaptureSession:captureSession]) {
        NSLog(@"failed to add camera input to capture session");
    }
    if (![self addDefaultMicInputToCaptureSession:captureSession]) {
        NSLog(@"failed to add mic input to capture session");
    }
    
    return captureSession;
}

- (BOOL)addDefaultCameraInputToCaptureSession:(AVCaptureSession *)captureSession {
    NSError *error;
    AVCaptureDeviceInput *cameraDeviceInput = [[AVCaptureDeviceInput alloc] initWithDevice:[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo] error:&error];

    if (error) {
        NSLog(@"error configuring camera input: %@", [error localizedDescription]);
        return NO;
    } else {
        BOOL success = [self addInput:cameraDeviceInput toCaptureSession:captureSession];
        cameraDevice = cameraDeviceInput.device;
        return success;
    }
}

//Not used in this project, but illustration of how to select a specific camera
- (BOOL)addCameraAtPosition:(AVCaptureDevicePosition)position toCaptureSession:(AVCaptureSession *)captureSession {
    AVCaptureDeviceInput *cameraDeviceInput;
    
    NSError *error;
    for (AVCaptureDevice *device in videoDevices) {
        if (device.position == position) {
            cameraDeviceInput = [[AVCaptureDeviceInput alloc] initWithDevice:device error:&error];
        }
    }
    if (!cameraDeviceInput) {
        NSLog(@"No capture device found for requested position");
        return NO;
    }
    
    if (error) {
        NSLog(@"error configuring camera input: %@", [error localizedDescription]);
        return NO;
    } else {
        BOOL success = [self addInput:cameraDeviceInput toCaptureSession:captureSession];
        videoDeviceInput = cameraDeviceInput;
        cameraDevice = cameraDeviceInput.device;
        return success;
    }
}

- (BOOL)addDefaultMicInputToCaptureSession:(AVCaptureSession *)captureSession {
    NSError *error;
    AVCaptureDeviceInput *micDeviceInput = [[AVCaptureDeviceInput alloc] initWithDevice:[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio] error:&error];
    if (error) {
        NSLog(@"error configuring mic input: %@", [error localizedDescription]);
        return NO;
    } else {
        BOOL success = [self addInput:micDeviceInput toCaptureSession:captureSession];
        return success;
    }
}

- (BOOL)changeCamera {
        AVCaptureDevice* currentVideoDevice = videoDeviceInput.device;
        AVCaptureDevicePosition currentPosition = currentVideoDevice.position;
        AVCaptureDevicePosition preferredPosition;
        AVCaptureDeviceType preferredDeviceType;
        
        switch (currentPosition) {
            case AVCaptureDevicePositionUnspecified:
            case AVCaptureDevicePositionFront:
                preferredPosition = AVCaptureDevicePositionBack;
                preferredDeviceType = AVCaptureDeviceTypeBuiltInWideAngleCamera;
                break;
            case AVCaptureDevicePositionBack:
                preferredPosition = AVCaptureDevicePositionFront;
                preferredDeviceType = AVCaptureDeviceTypeBuiltInWideAngleCamera;
                break;
        }
    
        AVCaptureDevice *newVideoDevice = nil;
        // First, look for a device with both the preferred position and device type.
        for (AVCaptureDevice* device in videoDevices) {
            if (device.position == preferredPosition && [device.deviceType isEqualToString:preferredDeviceType]) {
                newVideoDevice = device;
                break;
            }
        }
        
        // Otherwise, look for a device with only the preferred position.
        if (!newVideoDevice) {
            for (AVCaptureDevice* device in videoDevices) {
                if (device.position == preferredPosition) {
                    newVideoDevice = device;
                    break;
                }
            }
        }
        
        if (newVideoDevice) {
            AVCaptureDeviceInput* videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:newVideoDevice error:NULL];
            // Remove the existing device input first, since using the front and back camera simultaneously is not supported.
            [self removeInput:videoDeviceInput fromCaptureSession:captureSession];
            [self addInput:videoDeviceInput toCaptureSession:captureSession];
            self.videoDeviceInput = videoDeviceInput;
        }
    
    return YES;
}

- (BOOL)addInput:(AVCaptureDeviceInput *)input toCaptureSession:(AVCaptureSession *)captureSession {
    if ([captureSession canAddInput:input]) {
        [captureSession addInput:input];
        return YES;
    } else {
        NSLog(@"can't add input: %@", [input description]);
    }
    return NO;
}

- (void)removeInput:(AVCaptureDeviceInput *)input fromCaptureSession:(AVCaptureSession *)captureSession {
    [captureSession removeInput:input];
}


- (BOOL)addOutput:(AVCaptureOutput *)output toCaptureSession:(AVCaptureSession *)captureSession {
    if ([captureSession canAddOutput:output]) {
        [captureSession addOutput:output];
        return YES;
    } else {
        NSLog(@"can't add output: %@", [output description]);
    }
    return NO;
}


#pragma mark - Methods discussed in the article but not used in this demo app

- (void)setFrameRateWithDuration:(CMTime)frameDuration OnCaptureDevice:(AVCaptureDevice *)device {
    NSError *error;
    NSArray *supportedFrameRateRanges = [device.activeFormat videoSupportedFrameRateRanges];
    BOOL frameRateSupported = NO;
    for(AVFrameRateRange *range in supportedFrameRateRanges){
        if(CMTIME_COMPARE_INLINE(frameDuration, >=, range.minFrameDuration) && CMTIME_COMPARE_INLINE(frameDuration, <=, range.maxFrameDuration)){
            frameRateSupported = YES;
        }
    }
    
    if (frameRateSupported && [device lockForConfiguration:&error]) {
        [device setActiveVideoMaxFrameDuration:frameDuration];
        [device setActiveVideoMinFrameDuration:frameDuration];
        [device unlockForConfiguration];
    }
}


- (void)listCamerasAndMics {
//    NSLog(@"%@", [[AVCaptureDevice devices] description]);
    NSError *error;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    [audioSession setActive:YES error:&error];
    
    NSArray *availableAudioInputs = [audioSession availableInputs];
    NSLog(@"audio inputs: %@", [availableAudioInputs description]);
    for (AVAudioSessionPortDescription *portDescription in availableAudioInputs) {
        NSLog(@"data sources: %@", [[portDescription dataSources] description]);
    }
    if ([availableAudioInputs count] > 0) {
        AVAudioSessionPortDescription *portDescription = [availableAudioInputs firstObject];
        if ([[portDescription dataSources] count] > 0) {
            NSError *error;
            AVAudioSessionDataSourceDescription *dataSource = [[portDescription dataSources] lastObject];
            
            [portDescription setPreferredDataSource:dataSource error:&error];
            [self logError:error];
            
            [audioSession setPreferredInput:portDescription error:&error];
            [self logError:error];

            NSArray *availableAudioInputs = [audioSession availableInputs];
            NSLog(@"audio inputs: %@", [availableAudioInputs description]);
        }
    }
}

- (void)logError:(NSError *)error {
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
}

- (void)configureFrontMic {
    NSError *error;
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    [audioSession setActive:YES error:&error];
    
    NSArray* inputs = [audioSession availableInputs];
    AVAudioSessionPortDescription *builtInMic = nil;
    for (AVAudioSessionPortDescription* port in inputs) {
        if ([port.portType isEqualToString:AVAudioSessionPortBuiltInMic]) {
            builtInMic = port;
            break;
        }
    }
    
    for (AVAudioSessionDataSourceDescription* source in builtInMic.dataSources) {
        if ([source.orientation isEqual:AVAudioSessionOrientationFront]){
            [builtInMic setPreferredDataSource:source error:nil];
            [audioSession setPreferredInput:builtInMic error:&error];
            break;
        }
    }
}


@end
