#import "PermissionModel.h"

@implementation PermissionModel
@synthesize title, message, granted;

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
                      granted:(BOOL)granted {
    if (self = [super init]) {
        self.title = title;
        self.message = message;
        self.granted = granted;
    }
    
    return self;
}

- (instancetype)initWithGranted:(BOOL)granted {
    if (self = [super init]) {
        self.granted = granted;
    }
    
    return self;
}

@end
