#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PermissionModel : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *message;
@property (nonatomic) BOOL granted;

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
                      granted:(BOOL)granted;

- (instancetype)initWithGranted:(BOOL)granted;

@end

NS_ASSUME_NONNULL_END
